---
title: Conference Venue and Hotel
headline: 'Conference Venue and Hotel'
container: container margin-top-20 margin-bottom-20
---

[Hotel Königshof](https://www.hotel-koenigshof-garmisch.de/) serves as both the conference venue and hotel.
It is centrally located in [Garmisch-Partenkirchen](https://www.gapa-tourismus.de/en). Amenities at the
four-star hotel include an in-house spa with sauna and fitness studio. The area is a prominent destination
for hiking in the summer and skiing in the winter. Close to the town is [Zugspitze](https://zugspitze.de/en),
Germany’s highest mountain.

<!-- Images -->

{{< grid/div class="hotel-img-row gap-10 margin-top-20 margin-bottom-20" isMarkdown="false" >}}
    <div class="hotel-img-wrapper">
        <img src="images/exterior.jpg" alt="The view from the exterior of Hotel Königshof">
    </div>
    <div class="hotel-img-wrapper">
        <img src="images/deluxe-room.jpg" alt="A deluxe room with a king sized bed. A sofa and an armchair around a coffee table and a television.">
    </div>
{{</ grid/div >}}

A limited number of hotel rooms are being offered at a special rate for attendees for the nights of 5 June and 6 June. **The rooms will be available through 23 April, 2023**. Please be sure to book by this date to secure your lodging.

- Rate per night: 139 € (VAT included) - includes breakfast, spa and fitness facilities
- Parking per night: 10 €

To book, email <verkauf@hotel-koenigshof-garmisch.de> and include "Eclipse Foundation" in your email. You will need to provide a credit card to secure your reservation. Please be sure to read the reservation cancellation policy listed below. You may also reserve by calling [+49 (0) 8821 914-0](tel:4988219140).

## Contact Information

[Hotel Königshof](https://www.hotel-koenigshof-garmisch.de/)

St.-Martin-Strasse 4

82467 Garmisch-Partenkirchen

[+49 (0) 8821 914-0](tel:4988219140)

## Cancellation Policy

- 3 weeks prior to arrival: no cancellation fee
- 2 weeks prior to arrival: 50% cancellation fee
- 1 week prior to arrival: 80% cancellation fee
- 1 day prior to arrival or no-show: 100% cancellation fee