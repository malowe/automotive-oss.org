---
title: Automotive Open Source Summit 2023
seo_title: Automotive Open Source Summit 2023
date: 2022-11-30T10:00:00-04:00
headline: 'Automotive Open Source Summit 2023'
custom_jumbotron: |
    <div class="automotive-oss-jumbotron-2022">
        <div class="jumbotron-head">
            <div class="jumbotron-title-container">
                <p class="jumbotron-subtitle">Driving Open Source Innovation in Automotive Grade Software</p>
                <p class="jumbotron-details-text">6 June 2023 | Hotel Königshof, Garmisch-Partenkirchen, Germany</p>
            </div>
        </div>
    </div>
header_wrapper_class: "header-automotive-oss-event-2023"
summary: 'The Automotive Open Source Summit is an annual gathering of senior architects, innovators, and executives from around the world. At this event, attendees come together to discuss the latest trends in automotive grade open source software, network with like-minded professionals, and learn about the various technologies and challenges faced in automotive software development. As the inaugural event, the 2023 Automotive Open Source Summit will be focused around topics for senior architects and executives, with the outlook of expanding the conversation towards developers and engineers in the future. The Automotive Open Source Summit is a great place for automotive professionals to gain knowledge, keep up to date with the latest developments, and make valuable connections in the industry.'
categories: []
keywords: ["Open Source Automotive Software","Open Source Automotive Technology","Automotive Software Conference","Automotive Software Development","Vehicle Software Engineering","Automotive Software","Automotive Technology","Automotive Software Communities"]
slug: ""
aliases: []
hide_breadcrumb: true
container: container-fluid
layout: single 
cascade:
    header_wrapper_class: header-automotive-oss-event-2023
    hide_sidebar: true
    hide_page_title: true
    page_css_file: /public/css/automotive-oss-event-2023.css
---

<!-- About and Registration -->
{{< grid/div isMarkdown="false" class="row white-row padding-top-40 featured-story" >}}
{{< grid/div id="about" isMarkdown="false" class="container automotiveoss-ctn" >}}
{{< events/registration event="default" year="2023" >}}
{{</ events/registration >}}
{{</ grid/div >}}
{{</ grid/div >}}

<!-- Additional Info -->
{{< grid/section-container id="additional-info" class="featured-section-row featured-section-row-lighter-bg padding-top-60 padding-bottom-60 text-center featured-story" isMarkdown="false" >}}
    <h2>Additional Information</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <h3>Come Early to Meet the Community!</h3>
            <p>
                To take advantage of Garmisch’s beautiful setting in the Alps, we are planning some fun activities for the day before the Summit (5 June). 
                If you would like to join, please indicate your interest on the registration page, and plan to arrive a day early. Details are coming soon. 
            </p>
        </div>
        <div class="col-md-10">
            <h3>Lodging</h3>
            <p>
                We have a block of rooms reserved at Hotel Königshof at a special rate for conference attendees.
                For more information or to book, please visit the <a href="/2023/hotel-and-venue/">Hotel & Venue page</a>.
            </p>
        </div>
    </div>
{{</ grid/section-container >}}

<!-- Program -->
{{< grid/div id="program" isMarkdown="false" class="container padding-top-60 padding-bottom-60 text-center" >}}
    {{< grid/div isMarkdown="false" class="row" >}}
        <h2>Program</h2>
        <p class="padding-bottom-20">The agenda focuses on the following topics:</p>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/discover/1.jpg" alt="SDV & Open Source Cultural & organisational Challenges">
            <figcaption class="h3">
                Open Source Cultural and Organizational Challenges
            </figcaption>
        </figure>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/discover/2.jpg" alt="The role of Open Source in Automotive Software">
            <figcaption class="h3">
                The Role of Open Source in Automotive Software
            </figcaption>
        </figure>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/discover/3.jpg" alt="Automotive-grade Open Source Software Stack">
            <figcaption class="h3">
                The Automotive-Grade Open Source Software Stack
            </figcaption>
        </figure>
    {{</ grid/div >}}
{{</ grid/div >}}

<!-- Speakers -->
{{< grid/section-container class="speakers-section featured-section-row-dark-bg padding-top-40 padding-bottom-40 text-center" id="speakers" >}}
    {{< events/user_display title="Speakers" event="default" year="2023" source="speakers" imageRoot="images/speakers/" subpage="speakers" displayLearnMore="false" >}}
    {{</ events/user_display >}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="row featured-section-row featured-section-row-light-bg padding-top-60 padding-bottom-60" isMarkdown="false" >}}
    {{< events/agenda event="default" year="2023" >}}
    <small>* All times displayed are in Central European Summer Time (CEST).</small>
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

{{< grid/div isMarkdown="false" class="row white-row featured-story padding-top-60 padding-bottom-60 text-center" >}}
    {{< grid/div id="about" isMarkdown="false" class="container automotiveoss-ctn" >}}
        <p class="big">If you’d like to get to know the community better, feel free to join us the day before for an additional day in Garmisch with some extra activities planned with the thriving SDV community members.</p>
    {{</ grid/div >}}
{{</ grid/div >}}

<!-- Organizers -->
{{< grid/section-container id="organizers" class="text-center featured-story">}}
  {{< events/sponsors event="default" year="2023" source="coorganizers" title="Co-organizers" useMax="false" displayBecomeSponsor="false" headerClass="padding-bottom-0" itemClass="padding-0" >}}
{{</ grid/section-container >}}
