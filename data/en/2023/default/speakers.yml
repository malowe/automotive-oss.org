items:
  - name: Uli Homann
    title: Corporate Vice President, Microsoft
    img: ulrich-homann.jpg
    bio: |
      Ulrich (Uli) Homann is a Corporate Vice President and Distinguished
      Architect in the Cloud + AI business at Microsoft. As part of the senior
      engineering leadership team, Uli is responsible for customer-led
      innovation efforts across the Cloud + AI investment portfolio engaging
      with the most complex and innovative customers and partners across the
      globe to enhance Microsoft products and services empowering organizations
      to achieve more. Uli has spent most of his career using well-defined
      applications and architectures to simplify and streamline the development
      of mission-critical business applications. He holds a bachelor of
      computer science and business administration from the University of
      Cologne, Germany.
  - name: Dr. Andreas Nauerz
    title: Executive Vice President, Products and Innovation, Robert Bosch GmbH
    img: andreas-nauerz.jpg
    bio: |
      <p>
        Since January 2023, Andreas Nauerz has been Executive Vice President,
        Products and Innovation at Bosch Digital. Additionally, he is Chief
        Expert, Distributed & Cloud Computing. With his modern leadership
        style, his experience in software development, and his passion for
        innovation, he contributes to transforming Bosch into a successful
        software company. For this purpose, he promotes expertise and drives
        cultural change. Previously, he was CEO & CTO of Bosch.IO. Andreas
        started his career at Bosch in 2019 in Corporate Research. He studied
        computer science as well as law and holds a PhD in computer science.
      </p>
  - name: Ernst Stöckl-Pukall
    title: Head of Digitization and Industry 4.0 Department, BMWK 
    img: ernst-stockl-pukall.jpg
    bio: |
      <p>
        As Head of Division "Digitisation and Industrie 4.0" in the Department for
        Industrial Policy in the Federal Ministry for Economics and Energy Ernst
        Stoeckl-Pukall focuses on topics regarding a successful digitisation of the
        manufacturing sector in Germany. He is in charge of the Manufacturing-X
        Initiative and a number of research and support funding schemes related to the
        automotive sector, e.g. autonomous driving, and the digital transformation of
        the industry. He graduated in Economics at Ludwig Maximilans-University in
        Munich.
      </p>
  - name: Max Senges
    title: CEO & Headmaster, 42 Wolfsburg & 42 Berlin
    img: max-senges.jpg
    bio: |
      <p>
        Max Senges is the CEO and Headmaster of 42 Wolfsburg and www.42.berlin.
        Previously, he worked at Google, primarily on Research Partnerships and
        Internet Governance. Max holds a PhD and a Master's Degree in the
        Information and Knowledge Society Program from the Universitat Oberta
        de Catalunya (UOC) in Barcelona as well as a Masters in Business
        Information Systems from the University of Applied Sciences Wildau
        (Berlin).
      </p>
  - name: Markus Rettstatt
    title: Unit Lead, Mercedes-Benz Tech Innovation
    img: markus-rettstatt.jpg
    bio: |
      <p>
        After leading for more than 20 years in several positions and companies
        and driven by the relevance of software in the car, Markus Rettstatt
        took over the role as Unit Lead for Car Software Development more than
        three years ago at the largest Mercedes-Benz German IT subsidiary,
        Mercedes-Benz Tech Innovation. Markus's goal is to enable teams to have
        passion for their work, and to make software development more
        efficient.
      </p>
  - name: Damian Barnett
    title: CTO, Luxoft
    img: damian-barnett.jpg
    bio: |
      <p>
        Damian Barnett is the CTO for Luxoft Automotive. He is responsible for
        defining the overall technology, portfolio and partnership strategy to
        position Luxoft as an automotive innovator, accelerating the
        transformation to Software Defined Vehicles. Damian is a
        results-oriented global software engineering leader with over 25 years
        of experience in embedded software across multiple industries. Born in
        England, Damian moved to Germany at a young age and studied Electrical
        Engineering at the University of Applied Sciences in Nuremberg,
        graduating in 1997.
      </p>
  - name: Astor Nummelin Carlberg
    title: Executive Director, OpenForum Europe
    img: astor-nummeling-carlberg.jpg
    bio: |
      <p>
        Astor Nummelin Carlberg is OFE's Executive Director, responsible for the
        overall vision, activities of the organisation and policy development. He
        has extensive experience of European policy making processes,
        communications and network-building. Astor leads conversations on
        Europe's digital challenges and the role of open technologies in
        achieving its full potential. He sits on the board of APELL, the European
        Open Source Business Association. Prior to OFE, Astor worked in the
        European Parliament and he was educated at Middlebury College, the Free
        University of Berlin and Solvay Business School.
      </p>
  - name: Christian Hort
    title: Senior Vice President, Automotive, T-Systems
    img: christian-hort.jpg
    bio: |
      <p>
        As Senior Vice President, Dr. Christian Hort is responsible for the
        Automotive segment at T-Systems International GmbH. Customers include
        OEMs such as Mercedes-Benz & Daimler Trucks, Volkswagen, BMW,
        Stellantis, and leading suppliers, e.g. Continental, Schaeffler,
        Vitesco and Bosch. During the first nine years of his career, he worked
        in various development positions, corporate strategy, and sales at the
        Liechtenstein fastening specialist Hilti AG. After that he switched to
        the automotive supplier industry at Harman International. There he held
        various managerial positions in global sales for over 9 years. In his
        last role, he was responsible for the cross-divisional global business
        of Harman for Daimler AG as General Manager.
      </p>
  - name: Michael Niklas Höret
    title: Strategic Partnerships Manager, Continental
    img: michael-niklas-horet.jpg
    bio: |
      <p>
        Michael Niklas-Höret is an active member of the AUTOSAR Steering
        Committee representing Continental. He has been active in AUTOSAR
        Standardization since 2013 in various roles. He is interested in
        preparing the AUTOSAR standard to meet the challenges of the automotive
        industry and guiding the organization through the journey of the
        Software Defined Vehicle.
      </p>
  - name: Dr. Sören Frey
    title: Mercedes-Benz Tech Innovation
    img: soren-frey.jpg
    bio: |
      <p>
        After several positions in software development and gaining experience
        in different industries, Sören turned to the automotive domain about 10
        years ago and joined Mercedes-Benz Tech Innovation, the largest German
        IT subsidiary of Mercedes-Benz. He is passionate about creating
        first-class automotive software systems, bringing together different
        development teams and other internal and external stakeholders to
        achieve the best possible solutions.
      </p>
  - name: Graham Smethurst
    title: Infotainment and Communication Systems, BMW Group
    img: graham-smethurst.jpg
    bio: |
      <p>
        Graham Smethurst, formerly Chairman and President of GENIVI, now serves
        as a Board Member and Chairman at COVESA. With extensive experience at
        the BMW Group as General Manager for Infotainment systems, Graham's
        current role involves research leadership in E/E Architecture, data
        management, vehicle interfaces, and driving collaborative programs for
        data sharing between OEMs and third-party entities.
      </p>
  - name: Dr. Christian Mosch
    title: Managing Director, Industrial Digital Twin Assocation (IDTA)
    img: christian-mosch.jpg
    bio: |
      <p>
        Dr. Christian Mosch studied mechanical engineering at the TU Darmstadt
        and received his doctorate in the field of virtual product development.
        He became Managing Director at IDTA in 2021. Together with VDMA and
        ZVEI, he led the spin-off process to establish IDTA. Previously, he was
        responsible for Industry 4.0 strategy and standardization at VDMA.
        Christian Mosch is a member of the Plattform Industrie 4.0 Initiative.
      </p>
  - name: Martin Schleicher
    title: Head of Software Strategy, Continental
    img: martin-schleicher.jpg
    bio: |
      <p>
        Martin Schleicher has more than 25 years of experience in the
        automotive software business. Martin currently is Head of Software
        Strategy at Continental, aligning the company's activities related to
        the software defined vehicle. Before that he held various positions in
        Elektrobit, an automotive software company, including responsibility
        for product, portfolio management and partnerships.
      </p>
  - name: Gunther Bauer
    title: Senior Manager, ZF
    img: gunther-bauer.jpg
    bio: |
      <p>
        Gunther Bauer is a Senior Manager at ZF Group.  His work focuses on “Core
        Software” development of reuse software components for signal based and service
        oriented architectures at ZF Group within the global Software Center.  Gunther
        has more than 20 years of experience with development of software applications
        at ZF with a strong focus on software controls. He is also an integral member
        of the Eclipse Software Defined Vehicle Working Group.
      </p>
  - name: Peter Fintl
    title: Director Technology & Innovation, Capgemini Engineering
    img: peter-fintl.jpg
    bio: |
      <p>
        Peter Fintl is Vice President of Technology & Innovation at Capgemini.
        He holds degrees in Electrical Engineering and Business Administration.
        Peter started his career in aerospace/electronics but has now worked in
        automotive for over 20 years, holding various technical and managing
        positions in Europe and Canada. He spent over seven years in China
        supporting local and global automotive clients in product development
        and testing. Peter joined Capgemini eight years ago and is primarily
        involved in driver assistance systems/automated driving and related
        research programs.
      </p>
  - name: Markus Brandes
    title: Senior Partner, Head of Digital Product Engineering Services, IBM Consulting
    img: markus-brandes.jpg
    bio: |
      <p>
        Markus Brandes leads Digital Product Engineering Services in IBM
        Consulting EMEA. He and his teams co-create digital products/services
        with clients, making sure they are built right and the result is
        successful.  This is done with a value mindset and with a user centric
        digitalization approach, leveraging AI, Cloud and Open Source. Markus
        has 28 years of experience in software engineering and consulting,
        advising European automotive, manufacturing and industrial clients on
        building value-oriented, product-minded teams that can produce great
        digital products and services, resulting in positive business impacts
        for both their own businesses and those of  their partners. He holds a
        degree in computer science from the University of Stuttgart (Germany)
        and studied at the University of Oregon (US) and the SAID business
        school in Oxford (UK).
      </p>
  - name: Joachim Ritter
    title: Product Management Vehicle & Cloud Platform, CARIAD
    img: joachim-ritter.jpg
    bio: |
      <p>
        With over 20 years of experience in his field, Joachim is a tech
        executive who is passionate about product management and organizational
        development. He is one of the founding team members of CARIAD SE, where
        he currently focuses on strategy and product management for the Vehicle
        & Cloud Platform portfolio, including VW.OS, Clouds, OTA, Data
        Platforms, Connectivity, and more. Before joining CARIAD, Joachim
        served as the Chief Product Officer at 1&1 IONOS, overseeing a variety
        of large-scale SaaS applications and cloud platform services for SMEs.
        Prior to his work in the cloud industry, Joachim was employed at
        ProSyst, an embedded IoT startup that has since been acquired by Bosch.
        He began his career as a Software Engineer at Braun, a subsidiary of
        Gillette.
  - name: Daniel Miehle
    img: daniel-miehle.jpg
    title: CTO, Catena-X
    bio: |
      <p>
        Daniel Miehle is product manager at the BMW Group with a strong
        background in the automotive and information technology industries. His
        work focuses on leveraging digitalization and data-driven solutions to
        optimize business processes and enhance collaboration within complex
        ecosystems. Currently he is serving as the managing director for value
        creation & technology in the Catena-X Automotive Network e.V., working
        towards revolutionizing the automotive industry.
      </p>
